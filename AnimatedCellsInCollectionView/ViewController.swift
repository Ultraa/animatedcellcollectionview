//
//  ViewController.swift
//  AnimatedCellsInCollectionView
//
//  Created by User Userovich on 19.01.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController {
    
    var items = [Item]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.setCollectionViewLayout(CustomFlowLayout(), animated: false)
        for _ in 0...10 { addItem() }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "ItemCell",
            for: indexPath
        )
        
        cell.contentView.backgroundColor = items[indexPath.item].color
        
        return cell
    }
    
    func addItem() { items.append(Item(color: .random())) }

    @IBAction func addItem(_ sender: UIBarButtonItem) {
        addItem()
        
        let indexPath = IndexPath( item: self.items.count - 1, section: 0 )
        
        collectionView?.performBatchUpdates({ self.collectionView?.insertItems(at: [indexPath]) }, completion: nil)
    }
    
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red: CGFloat(drand48()),
                       green: CGFloat(drand48()),
                       blue: CGFloat(drand48()),
                       alpha: 1.0
        )
    }
}

struct Item {
    var color: UIColor
}
